﻿using FlowerApp.Models;
using FlowerApp.Pages;
using FlowerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerApp.Services
{
    public class NavigationService
    {
        public async Task Navigate(string pageName)
        {
            var mainViewModel = MainViewModel.GetInstance();

            switch (pageName)
            {
                case "NewFlowerPage":
                    mainViewModel.NewFlower = new NewFlowerViewModel();
                    await App.Current.MainPage.Navigation.PushAsync(new NewFlowerPage());
                    break;
                default:
                    break;
            }
        }

        public async Task EditFlower(Flower flower)
        {
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.EditFlower = new EditFlowerViewModel(flower);
            await App.Current.MainPage.Navigation.PushAsync(new EditFlowerPage());
        }


        public async Task Back()
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }

    }
}

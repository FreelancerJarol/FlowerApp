﻿using FlowerApp.Models;
using FlowerApp.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using Xamarin.Forms;
using Plugin.Media.Abstractions;
using Plugin.Media;
using FlowerApp.Classes;

namespace FlowerApp.ViewModels
{
    public class EditFlowerViewModel : Flower, INotifyPropertyChanged
    {
        #region Atributes
        private DialogService dialogService;
        private ApiService apiService;
        private NavigationService navigationService;
        public event PropertyChangedEventHandler PropertyChanged;
        private bool isRunning;
        private bool isEnabled;
        private ImageSource imageSource;
        private MediaFile file;
        #endregion

        #region Properties
        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }
        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }

        public ImageSource ImageSource
        {
            set
            {
                if (imageSource != value)
                {
                    imageSource = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ImageSource"));
                }
            }
            get
            {
                return imageSource;
            }
        }

        #endregion

        #region Events
        public ICommand DeleteFlowerCommand { get { return new RelayCommand(DeleteFlower); } }

        #endregion

        #region Contructors

        public EditFlowerViewModel(Flower flower)
        {
            dialogService = new DialogService();
            apiService = new ApiService();
            navigationService = new NavigationService();

            Description = flower.Description;
            Price = flower.Price;
            FlowerId = flower.FlowerId;
            LastPurchase = flower.LastPurchase;
            IsActive = flower.IsActive;
            Observation = flower.Observation;
            Image = flower.Image;

            IsEnabled = true;
        }

        #endregion

        #region Commands

        public ICommand TakePictureCommand { get { return new RelayCommand(TakePicture); } }
        private async void TakePicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await App.Current.MainPage.DisplayAlert("No Camera", ":( No camera available.", "Aceptar");
            }

            file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Sample",
                Name = "test.jpg",
                PhotoSize = PhotoSize.Small,
            });

            IsRunning = true;

            if (file != null)
            {
                ImageSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
            }

            IsRunning = false;
        }


        public ICommand SaveFlowerCommand { get { return new RelayCommand(SaveFlower); } }

        private async void SaveFlower()
        {
            if (string.IsNullOrEmpty(Description))
            {
                await dialogService.ShowMessage("Error", "Ingrese una descripción");
                return;
            }
            if (Price <= 0)
            {
                await dialogService.ShowMessage("Error", "Ingrese un valor positivo mayor a cero");
                return;
            }

            // Devuelve el arreglo en Bites
            var imageArray = FilesHelper.ReadFully(file.GetStream());
            file.Dispose();

            var flower = new Flower
            {
                FlowerId = FlowerId,
                Image = Image,
                Description = Description,
                Price = Price,
                IsActive = IsActive,
                LastPurchase = LastPurchase,
                Observation = Observation,
                ImageArray = imageArray,
            };

            IsRunning = true;
            IsEnabled = false;
            var response = await apiService.Put("http://flowerback01062017.azurewebsites.net", "/api", "/Flowers", flower /*this*/);
            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error Modificar", response.Message);
                return;
            }

            await navigationService.Back();
        }

        private async void DeleteFlower()
        {
            var answer = await dialogService.ShowConfirm("Confirm", "Desea eliminar la flor");

            if (!answer)
            {
                return;
            }

            IsRunning = true;
            IsEnabled = false;
            var response = await apiService.Delete("http://flowerback01062017.azurewebsites.net", "/api", "/Flowers", this /*flower*/);
            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error Eliminar", response.Message);
                return;
            }

            await navigationService.Back();
        }

        #endregion

    }
}

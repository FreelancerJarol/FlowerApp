﻿using FlowerApp.Models;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System;
using FlowerApp.Services;

namespace FlowerApp.ViewModels
{
    public class FlowerItemViewModel : Flower
    {
        private NavigationService navigationService;

        public FlowerItemViewModel()
        {
            navigationService = new NavigationService();
        }

        public ICommand EditFlowerCommand { get { return new RelayCommand(EditFlower); } }

        private async void EditFlower()
        {
            await navigationService.EditFlower(this);
        }
    }
}

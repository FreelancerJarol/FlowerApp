﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using FlowerApp.Services;
using FlowerApp.Models;
using Xamarin.Forms;
using Plugin.Media.Abstractions;
using Plugin.Media;
using FlowerApp.Classes;

namespace FlowerApp.ViewModels
{
    public class NewFlowerViewModel : Flower, INotifyPropertyChanged
    {
        #region Atributes
        private DialogService dialogService;
        private ApiService apiService;
        private NavigationService navigationService;
        private bool isRunning;
        private bool isEnabled;
        private ImageSource imageSource;
        private MediaFile file;
        #endregion

        #region Properties
        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }
        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }
        public ImageSource ImageSource
        {
            set
            {
                if (imageSource != value)
                {
                    imageSource = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ImageSource"));
                }
            }
            get
            {
                return imageSource;
            }
        }

        #endregion

        #region Contructors
        public NewFlowerViewModel()
        {
            dialogService = new DialogService();
            navigationService = new NavigationService();
            apiService = new ApiService();

            LastPurchase = DateTime.Now;
            IsEnabled = true;

        }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Commands

        public ICommand TakePictureCommand { get { return new RelayCommand(TakePicture); } }
        private async void TakePicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await App.Current.MainPage.DisplayAlert("No Camera", ":( No camera available.", "Aceptar");
            }

            file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Sample",
                Name = "test.jpg",
                PhotoSize = PhotoSize.Small,
            });

            IsRunning = true;

            if (file != null)
            {
                ImageSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
            }

            IsRunning = false;
        }

        public ICommand NewFlowerCommand { get { return new RelayCommand(NewFlower) ; } }

        private async void NewFlower()
        {
            if (string.IsNullOrEmpty(Description))
            {
                await dialogService.ShowMessage("Error", "Ingrese una descripción");
                return;
            }
            if (Price <= 0)
            {
                await dialogService.ShowMessage("Error", "Ingrese un valor positivo mayor a cero");
                return;
            }

            // Devuelve el arreglo en Bites
            var imageArray = FilesHelper.ReadFully(file.GetStream());
            file.Dispose();

            var flower = new Flower
            {
                Description = Description,
                Price = Price,
                IsActive = IsActive,
                LastPurchase = LastPurchase,
                Observation = Observation,
                ImageArray = imageArray,
            };

            IsRunning = true;
            IsEnabled = false;

            var response = await apiService.Post("http://flowerback01062017.azurewebsites.net", "/api", "/Flowers", flower);

            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error Insertar", response.Message);
                return;
            }

            await navigationService.Back();
        }

        #endregion
    }
}

﻿using FlowerApp.Models;
using FlowerApp.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;

namespace FlowerApp.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Attributes
        private ApiService apiService;
        private NavigationService navigationService;
        private DialogService dialogService;
        private bool isRefreshing;
        #endregion

        #region Properties
        public ObservableCollection<FlowerItemViewModel> Flowers { get; set; }
        public NewFlowerViewModel NewFlower { get; set; }
        public EditFlowerViewModel EditFlower { get; set; }
        public bool IsRefreshing
        {
            set
            {
                if (isRefreshing != value)
                {
                    isRefreshing = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRefreshing"));
                }
            }
            get
            {
                return isRefreshing;
            }
        }

        #endregion

        #region Constructors
        public MainViewModel()
        {
            // Singleton
            instance = this;

            // Services
            apiService = new ApiService();
            navigationService = new NavigationService();
            dialogService = new DialogService();

            // View Models
            Flowers = new ObservableCollection<FlowerItemViewModel>();

            // Load Data
            //LoadFlowers(); Se quita porque hay otra parta donde se carga los datos FlowerPage InicialComponente
        }

        #endregion

        #region Methods
        private async void LoadFlowers()
        {
            var respose = await apiService.Get<Flower>("http://flowerback01062017.azurewebsites.net", "/api", "/Flowers");
            if (!respose.IsSuccess)
            {
                await dialogService.ShowMessage("Error", respose.Message);
                return;
            }
            ReloadFlower((List<Flower>)respose.Result);
        }

        private void ReloadFlower(List<Flower> flowers)
        {
            Flowers.Clear();
            foreach (var flower in flowers.OrderBy(f => f.Description))
            {
                Flowers.Add(new FlowerItemViewModel
                {
                    Description = flower.Description,
                    FlowerId = flower.FlowerId,
                    Price = flower.Price,
                    Image = flower.Image,
                    IsActive = flower.IsActive,
                    LastPurchase = flower.LastPurchase,
                    Observation = flower.Observation,
                });
            }
        }

        #endregion

        #region Commands

        public ICommand RefreshFlowersCommand { get { return new RelayCommand(RefreshFlowers); } }

        private void RefreshFlowers()
        {
            IsRefreshing = true;
            LoadFlowers();
            IsRefreshing = false;

        }

        public ICommand AddFlowerCommand { get { return new RelayCommand(AddFlower); } }

        private async void AddFlower()
        {
            await navigationService.Navigate("NewFlowerPage");
        }

        #endregion

        #region Singleton

        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                instance = new MainViewModel();
            }

            return instance;
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

    }
}

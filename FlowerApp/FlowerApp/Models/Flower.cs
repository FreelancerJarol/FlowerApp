﻿using System;

namespace FlowerApp.Models
{
    public class Flower
    {
        public int FlowerId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public DateTime? LastPurchase { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public string Observation { get; set; }
        public byte[] ImageArray { get; set; }
        public string ImageFullPath
        {
            get
            {
                if (string.IsNullOrEmpty(Image))
                {
                    return "FlowerDefault.png";
                }
                return string.Format("http://flowerback01062017.azurewebsites.net{0}", Image.Substring(1));
            }

        }
        // Para modificar y eliminar la flor necesita que el GetHashCode tomo le valor del id para que la url quede bien definida.
        public override int GetHashCode()
        {
            return FlowerId;
        }

    }
}
